//
//  ViewController.m
//  MorseRecognizer
//
//  Created by Valiantsin Vasiliavitski on 4/18/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "ViewController.h"
#import "MRGestureRecognizer.h"
#import "MRDictionary.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *recognizedText;
@property (weak, nonatomic) IBOutlet UILabel *recognizedSequence;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognized)];
//    MRGestureRecognizer *morseRecognizer = [[MRGestureRecognizer alloc] initWithTarget:self action:@selector(morseTapRecognized) viewController:self];
    MRGestureRecognizer *morseRecognizer = [[MRGestureRecognizer alloc] initWithTarget:self action:@selector(morseTapRecognized) block:^ (NSDictionary *dictionary){
        NSString *result = [dictionary valueForKey:self.recognizedSequence.text];
        if(result){
            
            self.recognizedText.text = [self.recognizedText.text stringByAppendingString:[dictionary valueForKey:self.recognizedSequence.text]];
        }
        self.recognizedSequence.text = @"";
    }];
    [self.view addGestureRecognizer:morseRecognizer];
    [self.view addGestureRecognizer:tapRecognizer];
    [tapRecognizer requireGestureRecognizerToFail:morseRecognizer];
    self.recognizedSequence.text = @"";
    self.recognizedText.text = @"";    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)morseTapRecognized {
    
    self.recognizedSequence.text = [self.recognizedSequence.text stringByAppendingString:@"-"];
}

- (void)tapRecognized {
    self.recognizedSequence.text = [self.recognizedSequence.text stringByAppendingString:@"."];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)timerWorkWithDictionary:(NSDictionary*)dictionary {
    
    NSString *result = [dictionary valueForKey:self.recognizedSequence.text];
    if(result){
        
        self.recognizedText.text = [self.recognizedText.text stringByAppendingString:[dictionary valueForKey:self.recognizedSequence.text]];
    }
    self.recognizedSequence.text = @"";
}
- (IBAction)clear:(id)sender {
    
    self.recognizedText.text = @"";
    self.recognizedSequence.text = @"";
    
}
- (IBAction)deleteLastCharacter:(id)sender {
    
    if ([self.recognizedText.text length] > 0) {
        self.recognizedText.text = [self.recognizedText.text substringToIndex:[self.recognizedText.text length] - 1];
    }
}


@end
