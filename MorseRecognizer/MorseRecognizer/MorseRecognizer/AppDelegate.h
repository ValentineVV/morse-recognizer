//
//  AppDelegate.h
//  MorseRecognizer
//
//  Created by Valiantsin Vasiliavitski on 4/18/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

