//
//  MRGestureRecognizer.h
//  MorseRecognizer
//
//  Created by Valiantsin Vasiliavitski on 4/18/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKit/UIGestureRecognizerSubclass.h"

@interface MRGestureRecognizer : UITapGestureRecognizer

@property (nonatomic, strong) NSDate *touchBeginning;
@property (nonatomic, strong) NSTimer *timer;

- (instancetype)initWithTarget:(id)target action:(SEL)action viewController:(UIViewController*)viewController;

- (instancetype)initWithTarget:(id)target action:(SEL)action block:(void (^) (NSDictionary*))block;

@end
