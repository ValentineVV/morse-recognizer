//
//  MRGestureRecognizer.m
//  MorseRecognizer
//
//  Created by Valiantsin Vasiliavitski on 4/18/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "MRGestureRecognizer.h"
#import "MRDictionary.h"
#import "ViewController.h"

@interface MRGestureRecognizer()

@property (nonatomic, weak) ViewController *viewController;
@property (nonatomic, copy) void (^block)(NSDictionary *);

@end

@implementation MRGestureRecognizer

@synthesize viewController;

- (instancetype)initWithTarget:(id)target action:(SEL)action viewController:(ViewController*)viewController {

    if (self = [super initWithTarget:target action:action]) {
        self.viewController = viewController;
    }
    return self;
}

- (instancetype)initWithTarget:(id)target action:(SEL)action block:(void (^) (NSDictionary*))block {
    
    if (self = [super initWithTarget:target action:action]) {
        self.block = block;
    }
    return self;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    self.numberOfTouchesRequired = 1;
    self.numberOfTapsRequired = 1;
    self.touchBeginning = [[NSDate alloc] init];
    
    [self.timer invalidate];
    self.timer = nil;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    if ([[[NSDate alloc] init] timeIntervalSinceDate:self.touchBeginning] >= 0.3) {
        self.state = UIGestureRecognizerStateRecognized;
    } else {
        self.state = UIGestureRecognizerStateFailed;
        
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
        NSDictionary *dictionary = [MRDictionary getMorseDictionary];
//        [self.viewController timerWorkWithDictionary:dictionary];
        self.block(dictionary);
        
    }];
}

@end
